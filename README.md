# NOTE

This repository is now obsolete. See this [pull
request](https://github.com/mdevctl/mdevctl/pull/36) for the current status of
the rust rewrite.
